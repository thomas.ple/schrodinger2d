!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
!! MODULE USED TO DEFINE PARAMETERS !!
!!         OF THE POTENTIAL         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module potentials
	implicit none
	abstract interface
		real(8) function func(X)
			implicit none
			real(8),intent(in)  :: X(:)
		end function func
	end interface

	procedure (func), pointer :: Pot => null ()

	!POTENTIAL PARAMETERS
		!MORSE AND DOUBLE MORSE POTENTIAL
		real(8) :: D=20.,alpha=2.5 
			!MORSE
			real(8) ::	xmax=2.5
			!DOUBLE MORSE
			real(8) :: d0=0.95, ADM=2.32e5,BDM=3.15,CDM=2.31e4,asym=1.
		!HARMONIC OSCILLATOR
		real(8), allocatable :: mass_HO(:),Omega0(:)
		!DOUBLE WELL
		real(8) :: V0=10., x0=0.5
		!QUARTIC OSCILLATOR
		real(8) :: QO=0.1
		!COUPLED OSCILLATORS
		real(8) :: c3=0,c4=0
	namelist/potential_parameters/D,alpha,xmax,Omega0,V0,x0,asym,QO,c3,c4
	
CONTAINS

subroutine read_pot_nml(mass,nmlfile,Ndim)	
	use atomic_units
	implicit none
	integer, intent(in) :: Ndim
	real(8),intent(in) :: mass(Ndim)
	character(len=200),intent(in) :: nmlfile
	
	allocate(mass_HO(Ndim),Omega0(Ndim))

	open(1,file=trim(nmlfile))
	read(1,nml=potential_parameters)
	close(1)
	!CONVERT IN A.U. AND ASSIGN IN GLOBAL pot_param
		D=D/kcalperMol
		alpha=alpha*bohr
		xmax=xmax/bohr
		Omega0=Omega0/cm1
		V0=V0/kcalperMol
		x0=x0/bohr
		d0=d0/bohr
		ADM=ADM/kcalperMol
		BDM=BDM*bohr
		CDM=CDM/(kcalperMol*bohr**6)
	
	mass_HO(:)=mass(:)
end subroutine read_pot_nml

! ASSIGN THE POINTER Pot TO THE RIGHT FUNCTION DEPENDING ON THE
! INPUT VARIABLE pot_name
subroutine assign_Pot(pot_name,Ndim)
	implicit none
	integer, intent(in) :: Ndim
	character(2), intent(in) :: pot_name
	
	!ASSIGN Pot AND dPot TO THE CHOSEN POTENTIAL (subprograms/potentials.f90)
	SELECT CASE(pot_name)
	CASE("DM")
		if(Ndim/=2)	stop "Ndim must be equal to 2 for this potential. Stopping execution..."
		Pot=>Pot_DM
	CASE("HO")
		Pot=>Pot_HO
	CASE("CO")
		if(Ndim/=2)	stop "Ndim must be equal to 2 for this potential. Stopping execution..."
		Pot=>Pot_CO
	CASE DEFAULT
		stop "You must choose a valid potential! Stopping execution..."
	END SELECT

end subroutine assign_Pot

!------------------------------------------------------------
!------------------------------------------------------------
! DEFINITION OF THE POTENTIALS
! (to add a new potential, the function must be conform to the abstract interface "func"
!  and be assigned to Pot in the function assign_Pot)

	! A-H-B Hydrogen bond (see J. Beutier PhD thesis)
	real(8) function Pot_DM(X)
		implicit none
		real(8),intent(in)  :: X(:)
		
		Pot_DM=D*( &
					exp(-2*alpha*(X(2)/2+X(1)-d0)) &
					-2*exp(-alpha*(X(2)/2+X(1)-d0)) + 1 &
					+(asym**2)*( &
						exp(-2*alpha*(X(2)/2-X(1)-d0)/asym) &
						-2*exp(-alpha*(X(2)/2-X(1)-d0)/asym) &
					) &
				) + ADM*exp(-BDM*X(2))-CDM/(X(2)**6)
				
		
	end function Pot_DM

	
	!HARMONIC POTENTIAL
	real(8) function Pot_HO(X)
		implicit none
		real(8),intent(in)  :: X(:)
		
		Pot_HO=0.5*sum(mass_HO(:)*(Omega0(:)*X(:))**2)
		
	end function Pot_HO

	
	!COUPLED HARMONIC OSCILLATORS
	real(8) function Pot_CO(X)
		implicit none
		real(8),intent(in)  :: X(:)
		
		Pot_CO=0.5*sum(mass_HO(:)*(Omega0(:)*X(:))**2) &
				+c3*(X(1)-X(2))**3 &
				+c4*(X(1)-X(2))**4
		
	end function Pot_CO


end module potentials

