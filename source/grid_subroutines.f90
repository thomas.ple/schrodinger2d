module grid_subroutines
	implicit none
	contains

!UPDATE THE DENSITY WITH POINT (x0,p0)
subroutine update_grid2D(x,xmin,xmax,xstep,y,ymin,ymax,ystep,grid,weight)
	implicit none
	real(8), intent(in) :: x,xmin,xmax,y,ymin,ymax,weight
	integer, intent(in) :: xstep,ystep
	real(8), intent(inout) :: grid(1:xstep,1:ystep)
	integer :: i,j

	i=nint((x-xmin)*(xstep-1)/(xmax-xmin))+1
	j=nint((y-ymin)*(ystep-1)/(ymax-ymin))+1
	if(i>0 .and. i<=xstep .and. j>0 .and. j<=ystep) grid(i,j)=grid(i,j)+weight

end subroutine update_grid2D

!NORMALIZE 2D GRID
subroutine normalize_grid2D(xmin,xmax,xstep,ymin,ymax,ystep,grid,norm)
	implicit none
	real(8), intent(in) :: xmin,xmax,ymin,ymax
	integer, intent(in) :: xstep,ystep
	real(8), intent(inout) :: grid(1:xstep,1:ystep),norm
	real(8) :: dx,dy
	integer :: i,j

	dx=abs(xmax-xmin)/(xstep-1)
	dy=abs(ymax-ymin)/(ystep-1)
	
	norm=0
	do j=1,ystep
		do i=1,xstep
			norm=norm+grid(i,j)
		enddo
	enddo
	norm=norm*dx*dy
	grid=grid/norm
	
end subroutine normalize_grid2D

! ROUTINES FOR MARGINALS DENSITIES
subroutine update_grid1D(x,xmin,xmax,xstep,grid,weight)
	implicit none
	real(8), intent(in) :: x,xmin,xmax,weight
	integer, intent(in) :: xstep
	real(8), intent(inout) :: grid(1:xstep)
	integer :: i

	i=nint((x-xmin)*(xstep-1)/abs(xmax-xmin))+1
	if(i>0 .and. i<=xstep) grid(i)=grid(i)+weight

end subroutine update_grid1D

subroutine normalize_grid1D(xmin,xmax,xstep,grid,norm)
	implicit none
	real(8), intent(in) :: xmin,xmax
	integer, intent(in) :: xstep
	real(8), intent(inout) :: grid(1:xstep),norm
	real(8) :: dx
	integer :: i,j

	dx=abs(xmax-xmin)/(xstep-1)
	
	norm=0
	do i=1,xstep
		norm=norm+grid(i)
	enddo
	norm=norm*dx
	grid=grid/norm
	
end subroutine normalize_grid1D

end module grid_subroutines