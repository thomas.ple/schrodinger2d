module param
	character (len=200) :: outdir
	character (len=2) :: pot_name="DM"
	!DEFINE SYSTEM CHARACTERISTICS
	integer, parameter :: Ndim=2
	integer :: nev
	real(8), parameter :: kb=1., hbar=1., pi=acos(-1.0)
	real(8) :: Temperature=1.,T,mass(2)
	!DEFINE GRID PARAMETERS
	real(8) :: xmin(2),xmax(2),pmax(2)
	integer :: xstep,pstep,plim(2)
	!DEFINE SPECTRUM PARAMETERS
	real(8) :: damp,OmMax,dOm
	integer :: Fstep
	logical :: retrieve_Psi=.FALSE., solve_schro=.TRUE.
	logical :: in_THz=.FALSE.

	namelist/PARAMETERS/outdir,mass,Temperature,pot_name,nev
	namelist/GRID_PARAMETERS/xmin,xmax,xstep,pmax
	namelist/SPECTRUM_PARAMETERS/damp,OmMax,dOm,in_THz
end module param