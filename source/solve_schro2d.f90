module solve_schro2D
	implicit none

contains

subroutine av (n, psi, hpsi, x, y, V, delta, mass)
	! this is where the physics resides: the rest is purely technical
	! numerical algebra stuff...
	! this subroutine computes H*PSI as output, given PSI as input
	! matrix H is defined by its non-zero elements only
	!use units ! use atomic units with hbar = 1.
	implicit none
	integer, intent(in)                  :: n
	real(8), dimension(n*n), intent(in)  :: psi
	real(8), dimension(n),   intent(in)  :: x, y
	real(8), dimension(n,n), intent(in)  :: V
	real(8), dimension(n*n), intent(out) :: hpsi
	real(8),                 intent(in)  :: delta(2),mass(2)
	integer               :: l1, l2, m, m1, m2, m3, m4
	real(8)              :: psimx,psimy

	hpsi = 0._8        ! initalization
	! the real stuff begins here
	!$OMP PARALLEL DO PRIVATE(l1, l2, m, m1, m2,m3,m4, psimx, psimy)
	do l1 = 1, n ; do l2=1,n
		m = l1 + (l2-1)*n
		psimx = 2*psi(m) ; psimy=2*psi(m)
		m1 = l1 + l2*n ! determine non-zero elements
		if ( m1 <= n*n .and. m1 > 0 ) psimy = psimy - psi(m1)
		m2 = l1+1 + (l2-1)*n
		if ( m2 <= n*n .and. m2 > 0 ) psimx = psimx - psi(m2)
		m3 = l1 + (l2-2)*n
		if ( m3 <= n*n .and. m3 > 0 ) psimy = psimy - psi(m3)
		m4 = l1-1 + (l2-1)*n
		if ( m4 <= n*n .and. m4 > 0 ) psimx = psimx - psi(m4)
		! compute H*PSI directly
		hpsi(m) = psimx/(2*mass(1)*delta(1)**2) + psimy/(2*mass(2)*delta(2)**2) &
					+ V(l1,l2)*psi(m)
	enddo ; enddo
	!$OMP END PARALLEL DO
end subroutine av

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine solve_2D(n,x,y,v,nconv,eigval,eigvec,av,dx,nev,mass)
	! solve 2D eigenproblem without bothering about physics
	! output is eigenvalues and eigenvectors (and number of converged values)
	! needs a local subroutine av to compute the product of matrix A times vector V
	! syntax of av :
	! subroutine av (n, psi, hpsi, x, y)
	! requires ARPACK for sparse matrix diagonalization
	implicit none
	integer, intent(in) :: n   ! number of discretization points in each direction
	integer, intent(in) :: nev ! number of required eigenvalues
	real(8), dimension(n),       intent(in)  :: x, y   ! discretization pts
	real(8), dimension(n,n),     intent(in)  :: v      ! potential
	integer,                     intent(out) :: nconv  ! # of converged eigenvals
	real(8), dimension(nev),     intent(out) :: eigval ! eigenvalues
	real(8), dimension(n,n,nev), intent(out) :: eigvec ! eigenvalues
	real(8),                     intent(in)  :: dx(2),mass(2)
	integer :: ncv, lworkl, maxitr = 500
	integer :: i, j, m, info, ido, ierr
	real(8) :: tol = 0.0_8, sigm
	integer, dimension(11) :: iparam, ipntr
	real(8), dimension(:,:), allocatable :: ve, d
	real(8), dimension(:), allocatable :: workl, workd, resid, ax
	logical, dimension(:), allocatable :: se
	logical :: rvec
	real(8) :: dnrm2 ! external function in BLAS library

	m = n*n ! problem size
	! initialize and allocate junk for ARPACK
	ncv = 5*nev ; lworkl = ncv*(ncv+8)
	info = 0 ; ido = 0
	iparam(1) = 1 ; iparam(3) = maxitr ; iparam(7) = 1
	allocate( ve(m,ncv), workl(lworkl), workd(3*m), d(ncv,2), resid(m),    &
			ax(m), se(ncv) )

	! start Arnoldi reverse communication algorithm
	print '("Starting the diagonalization procedure")'
	revcom : do
		call dsaupd ( ido, 'I', m, 'SA', nev, tol, resid, ncv, ve, m, iparam,    &
						ipntr, workd, workl, lworkl, info )   ! ARPACK subroutine
		if ( ido /= -1 .and. ido /= 1) exit revcom          ! convergence attained
		call av(n, workd(ipntr(1)), workd(ipntr(2)), x, y, v, dx, mass) ! local subroutine
	enddo revcom
	! end of reverse communication loop

	!i post-process
	if ( info < 0 ) then
		print *, ' ' ; print *, ' Error with _saupd, info = ', info
		print *, ' Check documentation in _saupd ' ; print *, ' '
		nconv = 0
	else
		rvec = .true.
		call dseupd ( rvec, 'All', se, d, ve, m, sigm, 'I', m, 'SA', nev,  &
				tol, resid, ncv, ve, m, iparam, ipntr, workd, workl, lworkl, ierr )
		if ( ierr /= 0) then
			print *, ' ' ; print *, ' Error with _seupd, info = ', ierr
			print *, ' Check the documentation of _seupd. ' ; print *, ' '
		else
			nconv =  iparam(5)
			do i=1, nconv
				call av(n, ve(1:m,i), ax, x, y, v, dx,mass)
				call daxpy(m, -d(i,1), ve(:,i), 1, ax, 1)
				d(i,2) = dnrm2(m, ax, 1)
				d(i,2) = d(i,2) / abs(d(i,1))
			enddo
		endif
	endif

	eigval = 0 ; eigvec = 0
	if ( nconv > 0 )  then
		eigval(1:nconv) = d(1:nconv,1)
		do i = 1, n ; do j = 1, n
			eigvec(i,j,1:nconv) = ve(i+n*(j-1),1:nconv)
		enddo ; enddo
	endif

	deallocate( ve, workl, workd, d, resid, ax, se )
	
end subroutine solve_2D

end module solve_schro2D
