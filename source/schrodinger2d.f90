!!!!!!!!!!!!!!!!!!!!!!!!!
!! INCLUDE SUBPROGRAMS !!
!!!!!!!!!!!!!!!!!!!!!!!!!	

program schrodinger2D
	use param
	use atomic_units
	use potentials, ONLY: Pot,read_pot_nml,assign_Pot
	use grid_subroutines
	use solve_schro2D
	implicit none
	character(200) :: nmlfile
	character(10) :: flags
	character(1) :: solve_decide
	real(8), allocatable :: x(:),y(:),V(:,:),Psi(:,:,:),Ener(:)
	real(8), allocatable :: margX(:),margY(:),WORK(:),margPx(:),margPy(:),margXY(:,:)
	REAL(8), allocatable :: imPsiP(:,:,:),rePsiP(:,:,:),rft(:), ift(:)
	real(8) :: dx(2),dp(2),partition,norm,E0
	real(8), allocatable :: stat_fac(:),omega(:),theta(:)
	real(8), allocatable :: spec(:), spectrumX(:), spectrumY(:)
	real(8) :: x1nm,x2nm,omnm
	integer :: i,j,k,nconv,ifail
	real :: timeStart,timeFinish
	logical :: file_exists
	real(8) :: Funit

	!READ NAMELIST (namelist file must be passed in 1st argument)
	call get_command_argument(1,nmlfile)
	open(1,file=trim(nmlfile))
		read(1,nml=PARAMETERS)
	close(1)
	open(1,file=trim(nmlfile))
		read(1,nml=GRID_PARAMETERS)
	close(1)
	open(1,file=trim(nmlfile))
		read(1,nml=SPECTRUM_PARAMETERS)
	close(1)
	!CONVERT IN A.U.
		write(*,*) "mass=",mass
		mass=mass*Mprot
		T=kb*Temperature/kelvin

		xmin=xmin/bohr
		xmax=xmax/bohr
		pmax=pmax*Mprot*fs/bohr

		if(in_THz) then
			Funit=THz/(2*pi)
		else
			Funit=cm1
		endif
		damp=damp/Funit
		OmMax=OmMax/Funit
		dOm=dOm/Funit

		if(pstep<xstep) pstep=xstep

	! READ POTENTIAL NAMELIST
	call read_pot_nml(mass,nmlfile,Ndim)
	call assign_Pot(pot_name,2)

	!GET FLAGS
	call get_command_argument(2,flags)
	if(flags(1:1)=="-") then
		if(index(flags,'r')>-1 .OR. index(flags,'R')>-1) then
			retrieve_Psi=.True.
			solve_schro=.False.
		endif
	endif

	call system("mkdir "//trim(outdir))

	call cpu_time(timeStart)

	!STORE DISCRETIZATION POINTS
	dx(:)=(/ ((xmax(i)-xmin(i))/(xstep-1), i=1,2) /)
	write(*,*) "dx=",dx*bohr,"Angstrom"
	allocate(x(xstep),y(xstep),V(xstep,xstep))
	x(:)=(/ (xmin(1)+(i-1)*dx(1), i = 1,xstep) /)
	y(:)=(/ (xmin(2)+(i-1)*dx(2), i = 1,xstep) /)
	!WRITE POTENTIAL
	open(10,file=trim(outdir)//"/potential.out")
	do i=1,xstep ; do j=1,xstep
		V(i,j)=Pot((/ x(i),y(j) /))
		write(10,*) x(i)*bohr,y(j)*bohr,V(i,j)
	enddo ; write(10,*) ; enddo
	close(10)

	!SOLVE SCHRODINGER 2D
	allocate(Psi(xstep,xstep,nev))
	allocate(Ener(nev))
	allocate(stat_fac(nev))
	Psi=0 ; Ener=0 ; stat_fac=0
	inquire(file=trim(outdir)//"/Energies.out",exist=file_exists)
	if(file_exists) inquire(file=trim(outdir)//"/Psi.out",exist=file_exists)

!-----------------------------------------------
	!RETRIEVE DATA IF NECESSARY
	if(retrieve_Psi) then
		if(file_exists) then
			!READ ENERGIES AND PSIs
			open(10,file=trim(outdir)//"/Energies.out")
			k=1
			do
				read(10,*,iostat=ifail) nconv,Ener(k),stat_fac(k)
				if(ifail/=0) exit
				k=k+1
			enddo
			nconv=k-1
			print '("Found ",i3," eigenstates")' , nconv
			close(10)
			partition=sum(exp(-Ener(1:nconv)/T))
			
			open(10,file=trim(outdir)//"/Psi.out")
			do i=1,xstep ; do j=1,xstep
				read(10,*) x1nm,x2nm,Psi(i,j,1:nconv)
			enddo ; read(10,*) ; enddo
			close(10)
		else
			write(*,*) "Could not find Energies and Psi files."
			write(*,*) "Solve Schrodinger ?(y/n)"
			read(*,*) solve_decide
			if(solve_decide=="y") then
				solve_schro=.TRUE.
			else
				stop "Stopping execution..."
			endif
		endif
	endif

!-----------------------------------------------
	!SOLVE SCHODINGER IF NECESSARY
	if(solve_schro) then
		!IF RESULT FILES FOUND, ASK BEFORE OVERWRITING
		if(file_exists) then
			write(*,*)
			write(*,*) "Found Energies and Psi files. Solving will overwrite them."
			write(*,*) "Solve Schrodinger anyway ?(y/n)"
			read(*,*) solve_decide
			if(solve_decide/="y") then
				write(*,*) "To retrieve those files, execute with '-r'."
				stop "Stopping execution..."
			endif
		endif
		!2D Schrodinger solver (solve_schro2D.f90)
		call solve_2D(xstep,x,y,V,nconv,Ener,Psi,av,dx,nev,mass)
		print '("Found ",i3," eigenstates")' , nconv

		!COMPUTE PARTITION FUNCTION AND THERMAL FACTORS
		partition=sum(exp(-Ener(1:nconv)/T))
		stat_fac(1:nconv)=exp(-Ener(1:nconv)/T)/partition

		!WRITE ENERGIES AND PSIs
		open(10,file=trim(outdir)//"/Energies.out")
		E0=Ener(1)
		do k=1,nconv
			Ener(k)=Ener(k)-E0
			write(10,*) k,Ener(k),stat_fac(k)
		enddo
		close(10)
		
		open(10,file=trim(outdir)//"/Psi.out")
		do i=1,xstep ; do j=1,xstep
			write(10,*) x(i)*bohr,y(j)*bohr,Psi(i,j,1:nconv)
		enddo ; write(10,*) ; enddo
		close(10)
	endif

!-----------------------------------------------
	!COMPUTE POSITION DISTRIBUTIONS
	write(*,*) "Computing position distributions"
	allocate(margX(xstep),margY(xstep),margXY(xstep,xstep))
	margX=0 ; margY=0 ; margXY=0
	open(10,file=trim(outdir)//"/density_margX.out")
	open(11,file=trim(outdir)//"/density_margY.out")
	open(12,file=trim(outdir)//"/density_margXY.out")
	do i=1,xstep ; do j=1,xstep
			margX(i)=margX(i)+sum(stat_fac(1:nconv)*abs(Psi(i,j,1:nconv))**2)
			margY(i)=margY(i)+sum(stat_fac(1:nconv)*abs(Psi(j,i,1:nconv))**2)
			margXY(i,j)=sum(stat_fac(1:nconv)*abs(Psi(i,j,1:nconv))**2)
	enddo ; enddo
	call normalize_grid1D(xmin(1)*bohr,xmax(1)*bohr,xstep,margX,norm)
	call normalize_grid1D(xmin(2)*bohr,xmax(2)*bohr,xstep,margY,norm)
	call normalize_grid2D(xmin(1)*bohr,xmax(1)*bohr,xstep,xmin(2)*bohr,xmax(2)*bohr,xstep,margXY,norm)
	do i=1,xstep
		write(10,*) x(i)*bohr,margX(i)
		write(11,*) y(i)*bohr,margY(i)
	enddo
	do j=1,xstep ; do i=1,xstep
		write(12,*) x(i)*bohr,y(j)*bohr,margXY(i,j)
	enddo ; write(12,*) ; enddo
	close(10) ; close(11) ; close(12)
	deallocate(margX,margY,margXY)

!-----------------------------------------------
	! COMPUTE PsiP(px,py)
	write(*,*) "Computing PsiP (FFT 2D)"
	pstep=xstep
	allocate(imPsiP(pstep,pstep,nconv),rePsiP(pstep,pstep,nconv))
	allocate(margXY(pstep,pstep),margPx(pstep),margPy(pstep))
	
	! FIND A PADDING SIZE THAT IS A POWER OF 2 AND WORKS WITH pmax
	plim(:)=nint(xstep*pi/(pmax(:)*dx(:)))	
	do j=1,2
		do i=1,22
			if(plim(j)<=2**i) then
				plim(j)=2**(i-1)
				exit
			endif
		enddo
	enddo		
	dp(:)=2*pi/(dx(:)*plim(:))
	pmax(:)=dp(:)*pstep/2

	rePsiP=0 ; imPsiP=0 ; margXY=0
	write(*,*) plim
	write(*,*) "dp=",dp*bohr/(Mprot*fs)
	
	! FFT 2D
	rePsiP=0 ; imPsiP=0
	allocate(rft(plim(1)),ift(plim(1)),WORK(plim(1)))
	do i=1,pstep
		do k=1,nconv
			rft=0 ; ift=0
			rft(1:xstep)=Psi(1:xstep,i,k)
			call C06FCF(rft,ift,plim(1),work,ifail)
			rePsiP(pstep/2+1:pstep,i,k)=rft(1:pstep/2)
			rePsiP(1:pstep/2,i,k)=rft(plim(1)-pstep/2+1:plim(1))
			imPsiP(pstep/2+1:pstep,i,k)=ift(1:pstep/2)
			imPsiP(1:pstep/2,i,k)=ift(plim(1)-pstep/2+1:plim(1))
		enddo
	enddo
	deallocate(rft,ift,work)
	allocate(rft(plim(2)),ift(plim(2)),WORK(plim(2)))
	do i=1,pstep
		do k=1,nconv
			rft=0 ; ift=0
			rft(1:pstep)=rePsiP(i,1:pstep,k)
			ift(1:pstep)=imPsiP(i,1:pstep,k)
			call C06FCF(rft,ift,plim(2),work,ifail)
			rePsiP(i,pstep/2+1:pstep,k)=rft(1:pstep/2)
			rePsiP(i,1:pstep/2,k)=rft(plim(2)-pstep/2+1:plim(2))
			imPsiP(i,pstep/2+1:pstep,k)=ift(1:pstep/2)
			imPsiP(i,1:pstep/2,k)=ift(plim(2)-pstep/2+1:plim(2))
		enddo
	enddo
	deallocate(rft,ift,work)

	!COMPUTE MOMENTUM DISTRIBUTIONS
	write(*,*) "Computing momentum distributions"
	margXY=0
	do k=1,nconv
		margXY(:,:)=margXY(:,:)+stat_fac(k)*(imPsiP(:,:,k)**2+rePsiP(:,:,k)**2)
	enddo
	margPx=0 ; margPy=0
	do i=1,pstep
		margPx(i)=sum(margXY(i,:))
		margPy(i)=sum(margXY(:,i))
	enddo
	
	!normalization
	call normalize_grid2D(-pmax(1)*bohr/(Mprot*fs),pmax(1)*bohr/(Mprot*fs),pstep &
			,-pmax(2)*bohr/(Mprot*fs),pmax(2)*bohr/(Mprot*fs),pstep,margXY,norm)
	call normalize_grid1D(-pmax(1)*bohr/(Mprot*fs),pmax(1)*bohr/(Mprot*fs),pstep,margPx,norm)
	call normalize_grid1D(-pmax(2)*bohr/(Mprot*fs),pmax(2)*bohr/(Mprot*fs),pstep,margPy,norm)

	!write output files
	open(10,file=trim(outdir)//"/density_margPx.out")
	open(11,file=trim(outdir)//"/density_margPy.out")
	do j=1,pstep
		write(10,*) (-pmax(1)+(j-1)*dp(1))*bohr/(Mprot*fs),margPx(j)
	enddo
	do j=1,pstep
		write(11,*) (-pmax(2)+(j-1)*dp(2))*bohr/(Mprot*fs),margPy(j)
	enddo
	deallocate(margPx,margPy)
	close(10) ; close(11)
	open(10,file=trim(outdir)//"/density_margPxPy.out")
	do j=1,pstep ; do i=1,pstep
			write(10,*) (-pmax(1)+(i-1)*dp(1))*bohr/(Mprot*fs),(-pmax(2)+(j-1)*dp(2))*bohr/(Mprot*fs),margXY(i,j)
	enddo ; write(10,*) ; enddo
	close(10)
	deallocate(margXY)

!-----------------------------------------------
	!COMPUTE SPECTRUM
	write(*,*) "Computing spectrum"
	Fstep=int(OmMax/dOm)+1
	allocate(omega(1:Fstep),theta(1:Fstep))
	allocate(spec(1:Fstep),spectrumX(1:Fstep),spectrumY(1:Fstep))
	spectrumX=0 ; spectrumY=0
	omega(:)=(/ ((i-1)*dOm, i=1,Fstep) /)
	theta(1)=0
	theta(2:Fstep)=hbar*omega(2:Fstep)*( 0.5 + 1/(exp( hbar*omega(2:Fstep)/T)-1) )
	do i = 1,nconv-1 ; do j = i+1, nconv
		omnm=(Ener(j)-Ener(i))/hbar
		x1nm=0.
		x2nm=0.
		do k=1,xstep
			x1nm=x1nm+sum(x(:)*psi(:,k,i)*psi(:,k,j))
			x2nm=x2nm+sum(y(:)*psi(k,:,i)*psi(k,:,j))
		enddo
		spec(:)=4*(theta(:)/hbar)*(stat_fac(i)-stat_fac(j))*damp &
			*(omnm/(damp**2+(omnm-omega(:))**2)+omnm/(damp**2+(omnm+omega(:))**2))
		spectrumX(:)=spectrumX(:)+spec(:)*abs(x1nm)**2
		spectrumY(:)=spectrumY(:)+spec(:)*abs(x2nm)**2
	enddo ; enddo
	!norm=(sum(spectrumX(:))+sum(spectrumY(:)))*dF*THz
	spectrumX(:)=spectrumX(:)/(sum(spectrumX(:))*dOm*Funit)
	spectrumY(:)=spectrumY(:)/(sum(spectrumY(:))*dOm*Funit)
	open(10,file=trim(outdir)//"/spectrum.out")
	if(in_THz) then
		write(10,*) "# Frequency unit: THz"
	else
		write(10,*) "# Omega unit: cm^-1"
	endif
	do i=1,Fstep
		write(10,*) (i-1)*dOm*Funit,spectrumX(i),spectrumY(i)
	enddo
	close(10)
	
	!END PROGRAM
	call cpu_time(timeFinish)
	write(*,*) "Job done in",timeFinish-timeStart,"s."

end program schrodinger2D
